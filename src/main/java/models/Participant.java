package models;

import javax.xml.bind.annotation.XmlAttribute;

public class Participant
{

  @XmlAttribute(name="ID")
  public int id;

  @XmlAttribute(name="Name")
  public String name;

  @XmlAttribute(name="Logo")
  public String logo;
}