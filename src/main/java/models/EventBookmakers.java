package models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class EventBookmakers
{

  @XmlElement(name="Bookmaker", type=EventBookmaker.class)
  public List<EventBookmaker> bookmakers = new ArrayList();
}