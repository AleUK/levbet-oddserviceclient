package models;

import javax.xml.bind.annotation.XmlAttribute;

public class Team
{

  @XmlAttribute
  public int ParticipantID;

  @XmlAttribute
  public String ParticipantName;

  @XmlAttribute
  public int Position;

  @XmlAttribute
  public int TotalPoints;

  @XmlAttribute
  public int TotalPlayed;

  @XmlAttribute
  public int TotalWon;

  @XmlAttribute
  public int TotalDraw;

  @XmlAttribute
  public int TotalLost;

  @XmlAttribute
  public int TotalScored;

  @XmlAttribute
  public int TotalConceded;

  @XmlAttribute
  public int TotalDifference;

  @XmlAttribute
  public int HomePoints;

  @XmlAttribute
  public int HomePlayed;

  @XmlAttribute
  public int HomeWon;

  @XmlAttribute
  public int HomeDraw;

  @XmlAttribute
  public int HomeLost;

  @XmlAttribute
  public int HomeScored;

  @XmlAttribute
  public int HomeConceded;

  @XmlAttribute
  public int HomeDifference;

  @XmlAttribute
  public int AwayPoints;

  @XmlAttribute
  public int AwayPlayed;

  @XmlAttribute
  public int AwayWon;

  @XmlAttribute
  public int AwayDraw;

  @XmlAttribute
  public int AwayLost;

  @XmlAttribute
  public int AwayScored;

  @XmlAttribute
  public int AwayConceded;

  @XmlAttribute
  public int AwayDifference;
}