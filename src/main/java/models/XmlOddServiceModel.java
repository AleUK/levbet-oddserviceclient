package models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="OddService")
public class XmlOddServiceModel
{
  Header header;
  Results results;

  @XmlElement(name="Header")
  public Header getHeader()
  {
    return this.header;
  }

  public void setHeader(Header header) {
    this.header = header;
  }

  @XmlElement(name="Results")
  public Results getResults() {
    return this.results;
  }

  public void setResults(Results results) {
    this.results = results;
  }
}