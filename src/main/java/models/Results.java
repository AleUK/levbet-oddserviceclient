package models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Results
{

  @XmlElement(name="Bookmaker", type=Bookmaker.class)
  public List<Bookmaker> bookmakers = new ArrayList();

  @XmlElement(name="Country", type=Country.class)
  public List<Country> countries = new ArrayList();

  @XmlElement(name="League", type=League.class)
  public List<League> leagues = new ArrayList();

  @XmlElement(name="Event", type=Event.class)
  public List<Event> events = new ArrayList();

  @XmlElement(name="Outcome", type=Outcome.class)
  public List<Outcome> outcomes = new ArrayList();
}