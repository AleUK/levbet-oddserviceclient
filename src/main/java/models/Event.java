package models;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

public class Event
{

  @XmlElement(name="EventID")
  public int eventID;

  @XmlElement(name="StartDate", type=Date.class)
  public Date startDate;

  @XmlElement(name="SportID")
  public int sportID;

  @XmlElement(name="LeagueID")
  public int leagueID;

  @XmlElement(name="LocationID")
  public int LocationID;

  @XmlElement(name="Status")
  public String Status;

  @XmlElement(name="LastUpdate", type=Date.class)
  public Date LastUpdate;

  @XmlElement(name="HomeTeam", type=Participant.class)
  public Participant homeTeam;

  @XmlElement(name="AwayTeam", type=Participant.class)
  public Participant awayTeam;
  EventBookmakers bookmakers;
  Outcomes outcomes;

  @XmlElement(name="Bookmakers")
  public EventBookmakers getBookmakers()
  {
    return this.bookmakers;
  }

  public void setBookmakers(EventBookmakers bookmakers) {
    this.bookmakers = bookmakers;
  }

  @XmlElement(name="Outcomes")
  public Outcomes getOutcomes()
  {
    return this.outcomes;
  }

  public void setOutcomes(Outcomes outcomes) {
    this.outcomes = outcomes;
  }
}