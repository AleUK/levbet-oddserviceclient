package models;

import javax.xml.bind.annotation.XmlAttribute;

public class EventBookmaker
{

  @XmlAttribute(name="ID")
  public int id;

  @XmlAttribute(name="Name")
  public String name;

  @XmlAttribute(name="IsLive")
  public boolean isLive;

  @XmlAttribute(name="IsStream")
  public boolean isStream;
}