package models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Outcomes
{

  @XmlElement(name="Outcome", type=Outcome.class)
  public List<Outcome> outcomes = new ArrayList();
}