package models;

import javax.xml.bind.annotation.XmlElement;

public class Header
{
  String status;
  String description;
  String timestamp;
  String clientTimestamp;

  @XmlElement(name="Status")
  public String getStatus()
  {
    return this.status;
  }
  public void setStatus(String Status) {
    this.status = Status;
  }
  @XmlElement(name="Timestamp")
  public String getTimestamp() {
    return this.timestamp;
  }
  public void setTimestamp(String Timestamp) {
    this.timestamp = Timestamp;
  }
  @XmlElement(name="clientTimestamp")
  public String getClientTimestamp() {
    return this.clientTimestamp;
  }
  public void setClientTimestamp(String clientTimestamp) {
    this.clientTimestamp = clientTimestamp;
  }
  @XmlElement(name="Description")
  public String getDescription() {
    return this.description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
}