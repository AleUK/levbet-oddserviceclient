package models;

import javax.xml.bind.annotation.XmlAttribute;

public class Outcome
{

  @XmlAttribute
  public int id;

  @XmlAttribute
  public String name;
}