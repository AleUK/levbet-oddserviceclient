package models;

import javax.xml.bind.annotation.XmlAttribute;

public class Bookmaker
{

  @XmlAttribute
  public int id;

  @XmlAttribute
  public String name;
}