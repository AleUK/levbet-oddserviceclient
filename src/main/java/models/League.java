package models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class League
{

  @XmlAttribute
  public int id;

  @XmlAttribute
  public String name;

  @XmlAttribute
  public int locationID;

  @XmlAttribute
  public int sportID;

  @XmlAttribute
  public String season;

  @XmlElement(name="Team", type=Team.class)
  public List<Team> teams = new ArrayList();
}