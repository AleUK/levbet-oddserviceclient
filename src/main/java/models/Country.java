package models;

import javax.xml.bind.annotation.XmlAttribute;

public class Country
{

  @XmlAttribute
  public int id;

  @XmlAttribute
  public String name;
}