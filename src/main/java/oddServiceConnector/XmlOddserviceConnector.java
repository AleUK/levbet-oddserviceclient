package oddServiceConnector;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import models.XmlOddServiceModel;

public class XmlOddserviceConnector {

  private String username = "";//"jake.list@levbet.net";
  private String userpassword = "";//"76605028";
  private String packageGuid = "";//"194fe546-9869-4276-a4ff-72326e610c99"; // there is another but not sure what for 393b60dd-7939-46ff-9435-2d63b7b02810
  private String serviceUrl = "";//"http://client.oddservice.com"; http://xml.oddservice.com/OS/OddsWebService.svc/

  public XmlOddserviceConnector(String url, String name, String password, String guid) {
    this.username = name;
    this.userpassword = password;
    this.packageGuid = guid;
    this.serviceUrl = url;
  }

  private String BuildQueryString(Map<?, ?> map) {
    StringBuilder sb = new StringBuilder();
    for (Map.Entry entry : map.entrySet()) {
      if (sb.length() > 0)
        sb.append("&");
      try
      {
        sb.append(String.format("%s=%s", new Object[] { 
          URLEncoder.encode((String)entry.getKey(), "UTF-8"), 
          URLEncoder.encode((String)entry.getValue(), "UTF-8") }));
      }
      catch (UnsupportedEncodingException e)
      {
        e.printStackTrace();
      }
    }
    return sb.toString();
  }

  private URLConnection Connect(String serviceUrl, String funcName, Map<?, ?> options) throws IOException {
    String query = serviceUrl + "/" + funcName + "?" + BuildQueryString(options);

    URL url = new URL(query);
    URLConnection urlConnection = url.openConnection();
    urlConnection.setRequestProperty("Accept-Encoding", "gzip");

    return urlConnection;
  }

  private XmlOddServiceModel GetModelData(URLConnection connection) throws JAXBException, IOException {
    JAXBContext jc = JAXBContext.newInstance(new Class[] { XmlOddServiceModel.class });

    Unmarshaller unmarshaller = jc.createUnmarshaller();

    InputStream response = connection.getInputStream();

    Object objData = unmarshaller.unmarshal(response);
    XmlOddServiceModel data = (XmlOddServiceModel)objData;

    return data;
  }

  public XmlOddServiceModel InvokeFunction(String funcName, String lang, Map<String, String> options) throws IOException, JAXBException
  {
    options.put("email", this.username);
    options.put("password", this.userpassword);
    options.put("guid", this.packageGuid);
    options.put("lang", lang);

    URLConnection connection = Connect(this.serviceUrl, funcName, options);

    XmlOddServiceModel data = GetModelData(connection);

    return data;
  }
}